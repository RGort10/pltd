#ifndef DEBUG_DEBUG_H
#define DEBUG_DEBUG_H

#include "framebuffer.h"

uint16_t game_setup(frameBuffer_t *buf);
void game_loop(frameBuffer_t *buf);


#endif