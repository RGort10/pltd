#ifndef PONG_PONG_H
#define PONG_PONG_H

#include "../framebuffer.h"

uint16_t game_setup(frameBuffer_t *buf);
void game_loop(frameBuffer_t *buf);


#endif