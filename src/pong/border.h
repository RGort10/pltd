#ifndef PONG_BORDER_H
#define PONG_BORDER_H

#include "../framebuffer.h"

void pong_renderBorder(frameBuffer_t *buf);

#endif