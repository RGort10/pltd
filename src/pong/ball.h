#ifndef PONG_BALL_H
#define PONG_BALL_H

#include "../framebuffer.h"
#include "pongTypes.h"

#define PONG_BALL_SIZE_X PONG_SCREEN_MULTIPLIER
#define PONG_BALL_SIZE_Y PONG_SCREEN_MULTIPLIER
#define PONG_BALL_DEFAULT_SPEED PONG_SCREEN_MULTIPLIER

void pong_moveBall(pong_ball_t *ball, pong_bat_t *bat0, pong_bat_t *bat1, pong_score_t *score);
void pong_renderBall(frameBuffer_t *buf, pong_ball_t *ball);

#endif