#ifndef PONG_TYPES_H
#define PONG_TYPES_H

#include <stdint.h>
#include <stdbool.h>
#include "../framebuffer.h"

#define PONG_SCREEN_MULTIPLIER 16
#define PONG_SCREEN_WIDTH SCREEN_WIDTH * PONG_SCREEN_MULTIPLIER
#define PONG_SCREEN_HEIGHT SCREEN_HEIGHT * PONG_SCREEN_MULTIPLIER

typedef struct {
  uint32_t x;
  uint32_t y;
  int16_t moveX;
  int16_t moveY;
} pong_ball_t;

typedef struct {
  uint32_t x;
  uint32_t y;
} pong_bat_t;

typedef struct {
  uint8_t left;
  uint8_t right;
} pong_score_t;

typedef struct {
  enum {RUNNING, LEFT_WON, RIGHT_WON} state;
} pong_state_t;

typedef struct {
  pong_ball_t ball;
  pong_bat_t bat0;
  pong_bat_t bat1;
  pong_score_t score;
  pong_state_t state;
} pong_game_t;

#endif