#ifndef INPUT_H
#define INPUT_H

#include <stdint.h>

#define INPUT_ANALOG_TRHESHOLD 45

typedef union {
  struct {
    uint8_t left_right:1;
    uint8_t left_left:1;
    uint8_t left_bot:1;
    uint8_t left_top:1;
    uint8_t right_right:1;
    uint8_t right_left:1;
    uint8_t right_bot:1;
    uint8_t right_top:1;

    uint8_t start:1;
    uint8_t : 7; // Fill rest of reg
  } input;
  uint16_t raw;
} input_digitalInputs_t;

typedef struct {
  uint16_t left;
  uint16_t right;
} input_analogInputs_t;

typedef struct {
  input_digitalInputs_t digital;
  input_analogInputs_t analog;
} input_inputs_t;

void input_setup(void);
input_inputs_t input_getInputs(void);

#endif