#ifndef HELPERCORE_H
#define HELPERCORE_H

#include <stdint.h>
#include "framebuffer.h"
#include "pico/util/queue.h"

typedef enum {
  GET_FRAMEBUFFER,
  PAINT_FRAMEBUFFER,
  COMMAND_UNKNOWN
} helper_queueCommand_t;

typedef struct {
  helper_queueCommand_t command;
  frameBuffer_t *frameBuffer;
} helper_queueEntry_t;

extern queue_t helper_callQueue;
extern queue_t helper_resultsQueue;

void helper_coreEntry(void);

#endif