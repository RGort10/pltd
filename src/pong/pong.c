#include "pong.h"
#include "pongTypes.h"
#include "ball.h"
#include "bat.h"
#include "border.h"
#include <stdlib.h>
#include "../input.h"
#include "../framebuffer.h"

pong_game_t game;

uint16_t game_setup(frameBuffer_t *buf) {
  game.ball.x = PONG_SCREEN_WIDTH / 2;
  game.ball.y = PONG_SCREEN_HEIGHT / 2;
  game.ball.moveX = (rand() & 1) ? PONG_BALL_DEFAULT_SPEED : -PONG_BALL_DEFAULT_SPEED;
  game.ball.moveY = (rand() & 1) ? 2 : -2;

  game.bat0.x = 6 * PONG_SCREEN_MULTIPLIER;
  game.bat0.y = PONG_SCREEN_HEIGHT / 2;

  game.bat1.x = (SCREEN_WIDTH - 7) * PONG_SCREEN_MULTIPLIER;
  game.bat1.y = PONG_SCREEN_HEIGHT / 2;

  game.score.left = 0;
  game.score.right = 0;

  frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 16 - 16, 22, 'P', FRAMEBUFFER_WHITE);
  frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 16     , 22, 'O', FRAMEBUFFER_WHITE);
  frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2          , 22, 'N', FRAMEBUFFER_WHITE);
  frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 + 16     , 22, 'G', FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 - 4 - 48, 44, 'B', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 - 4 - 40, 44, 'y', FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 - 4 - 24, 44, 'R', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 - 4 - 16, 44, 'e', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 - 4 - 8 , 44, 'n', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 - 4     , 44, 'z', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 + 4     , 44, 'o', FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 + 4 + 16, 44, 'G', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 + 4 + 24, 44, 'o', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 + 4 + 32, 44, 'r', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, SCREEN_WIDTH / 2 + 4 + 40, 44, 't', FRAMEBUFFER_WHITE);

  return 1500;
}

void game_loop(frameBuffer_t *buf) {
  input_inputs_t inputs;
  frameBuffer_fillBuffer(buf, FRAMEBUFFER_BLACK);
  pong_renderBorder(buf);

  inputs = input_getInputs();

  if(game.state.state == RUNNING) {
    pong_moveBall(&game.ball, &game.bat0, &game.bat1, &game.score);
    pong_moveBatAnalog(&game.bat0, inputs.analog.left);
    // pong_moveBat(&game.bat0, inputs.digital.input.left_top, inputs.digital.input.left_bot);
    pong_moveBat(&game.bat1, inputs.digital.input.right_top, inputs.digital.input.right_bot);
  }

  if(game.score.left > 8 || game.score.right > 8) {
    game.state.state = game.score.left > 8 ? LEFT_WON : RIGHT_WON;
    if(inputs.digital.input.start) {
      game.score.left = 0;
      game.score.right = 0;
      game.state.state = RUNNING;
      game.ball.y = (rand() % SCREEN_HEIGHT) * PONG_SCREEN_MULTIPLIER;
      game.ball.moveY = (rand() & 1) ? 2 : -2;
    }
  }

  frameBuffer_printChar(buf, (SCREEN_WIDTH / 2) - 5 - 16, 5, ((game.score.left / 10) % 10) + '0', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, (SCREEN_WIDTH / 2) - 5 - 8, 5, (game.score.left % 10) + '0', FRAMEBUFFER_WHITE);
  
  frameBuffer_printChar(buf, (SCREEN_WIDTH / 2) + 6 , 5, ((game.score.right / 10) % 10) + '0', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, (SCREEN_WIDTH / 2) + 6 + 8 , 5, (game.score.right % 10) + '0', FRAMEBUFFER_WHITE);

  if(game.state.state == RUNNING) {
    pong_renderBall(buf, &game.ball);
    pong_renderBat(buf, &game.bat0);
    pong_renderBat(buf, &game.bat1);
  }

  if(game.state.state == LEFT_WON) {
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 32, 22, 'L', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 16, 22, 'E', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2     , 22, 'F', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 + 16, 22, 'T', FRAMEBUFFER_WHITE);
  }

  if(game.state.state == RIGHT_WON) {
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 40, 22, 'R', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 24, 22, 'I', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 8 , 22, 'G', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 + 8 , 22, 'H', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 + 24, 22, 'T', FRAMEBUFFER_WHITE);
  }

  if(game.state.state == LEFT_WON || game.state.state == RIGHT_WON) {
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 24, 44, 'W', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 - 8 , 44, 'O', FRAMEBUFFER_WHITE);
    frameBuffer_printCharXL(buf, SCREEN_WIDTH / 2 + 8 , 44, 'N', FRAMEBUFFER_WHITE);
  }
}