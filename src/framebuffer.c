#include "framebuffer.h"
#include "font.h"

void frameBuffer_fillBuffer(frameBuffer_t *buf, uint16_t color) {
  for(uint32_t i = 0; i < SCREEN_HEIGHT * SCREEN_WIDTH; i++) {
    buf[i] = color;
  }
}

void frameBuffer_fillRectangle(frameBuffer_t *buf, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color) {
  if(x1 < 0) x1 = 0;
  if(x2 > SCREEN_WIDTH - 1) x2 = SCREEN_WIDTH - 1;
  if(y1 < 0) y1 = 0;
  if(y2 > SCREEN_HEIGHT - 1) y2 = SCREEN_HEIGHT - 1;

  for(uint32_t y = y1; y < y2; y++) {
    for(uint32_t x = x1; x < x2; x++) {
      buf[y*SCREEN_WIDTH + x] = color;
    }
  }
}

void frameBuffer_fillDashedRectangle(frameBuffer_t *buf, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t dashLength, uint16_t color) {
  if(x1 < 0) x1 = 0;
  if(x2 > SCREEN_WIDTH - 1) x2 = SCREEN_WIDTH - 1;
  if(y1 < 0) y1 = 0;
  if(y2 > SCREEN_HEIGHT - 1) y2 = SCREEN_HEIGHT - 1;
  
  for(uint32_t y = y1; y < y2; y++) {
    if((y / dashLength) % 2 == 1) {
      for(uint32_t x = x1; x < x2; x++) {
        buf[y*SCREEN_WIDTH + x] = color;
      }
    }
  }
}

void frameBuffer_printChar(frameBuffer_t *buf, uint16_t x1, uint16_t y1, char c, uint16_t color) {
  const uint8_t *charBitmap = font_getChar(c);

  for(uint32_t y = y1; y < y1 + FONT_HEIGHT; y++) {
    for(uint32_t x = x1; x < x1 + FONT_WIDTH; x++) {
      if(charBitmap[y - y1] & (1 << (x - x1))) buf[y*SCREEN_WIDTH + x] = color;
    }
  }
}

void frameBuffer_print_uint16(frameBuffer_t *buf, uint16_t x1, uint16_t y1, uint16_t val, uint16_t color) {
  uint8_t value;
  frameBuffer_printChar(buf, x1, y1, '0', color);
  frameBuffer_printChar(buf, x1 + FONT_WIDTH, y1, 'x', color);

  value = (val >> 12);
  if(value < 10) frameBuffer_printChar(buf, x1 + FONT_WIDTH * 2, y1, value + '0', color);
  else           frameBuffer_printChar(buf, x1 + FONT_WIDTH * 2, y1, value - 10 + 'A', color);

  value = (val >> 8) & 0xF;
  if(value < 10) frameBuffer_printChar(buf, x1 + FONT_WIDTH * 3, y1, value + '0', color);
  else           frameBuffer_printChar(buf, x1 + FONT_WIDTH * 3, y1, value - 10 + 'A', color);

  value = (val >> 4) & 0xF;
  if(value < 10) frameBuffer_printChar(buf, x1 + FONT_WIDTH * 4, y1, value + '0', color);
  else           frameBuffer_printChar(buf, x1 + FONT_WIDTH * 4, y1, value - 10 + 'A', color);

  value = val  & 0xF;
  if(value < 10) frameBuffer_printChar(buf, x1 + FONT_WIDTH * 5, y1, value + '0', color);
  else           frameBuffer_printChar(buf, x1 + FONT_WIDTH * 5, y1, value - 10 + 'A', color);
}

void frameBuffer_printCharXL(frameBuffer_t *buf, uint16_t x1, uint16_t y1, char c, uint16_t color) {
  const uint8_t *charBitmap = font_getChar(c);

  for(uint32_t y = y1; y < y1 + (FONT_HEIGHT * 2); y++) {
    for(uint32_t x = x1; x < x1 + (FONT_WIDTH * 2); x++) {
      if(charBitmap[(y - y1) / 2] & (1 << ((x - x1) / 2))) buf[y*SCREEN_WIDTH + x] = color;
    }
  }
}