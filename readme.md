# PLTD (Pico Living The Dream)
### Raspberry Pi Pico Code to display to a HX8357D screen

This code can write a framebuffer of 120x80 to a screen with an HX8357D controller. The screen used here is an 3.5" screen with an resolution of 320x480. Due to the insane high resolution a normal framebuffer would take up 320x480x2 = 307.2 kB if a 16-bit color is used. This is bigger than all of the picos RAM. By limiting the resolution to 120x80 the framebuffer size can be reduced to 120x80x2 = 19.2 kB.

This code is using the second core to draw the framebuffer to the screen. The DMA module is at this moment not used due to the way the framebuffer works. Maybe in the future if I find a way to make the DMA module to write the same column 4x the second core only has to initate the transfer. For now it has to do most of the work. 

## PIO
The second core uses a combination of normal SIO gpio operation and a PIO program. Due to the fact that the SIO is quite 'slow' a full framebuffer write takes about 50 to 60 milliseconds. This is only 16 to 20 FPS. By using a PIO program to write to the databus and strobe the WR bit. It can be reduced to only take about 11 milliseconds. That equals to a whopping 90 FPS. More than enough to have a nice experience. Keep in mind that at this speed you shouldn't be using to long jumper wires because you know electrical signals.

The overhead of the sio consists of a few parts: 

* It takes 5 c instuctions for the SIO code to write 1 byte. This includes the code necessary to shift the bits to the correct place to use the sio registers
* It takes time between a write of a byte (or multiple if you write more than 1 byte in a function call). to switch the functions. or handle for loop jumps and calculations.
* Other cpu related latencys.

The pio fixes a few of those issues:

* It only takes 4 pio instructions to write 1 byte. With an overhead of 3 instructions per 4 bytes to load the next byte. + The are no difficulties with shifting data to the correct place in the registers.
* Because the PIO has a 8 deep FIFO buffer (tx and rx combined because we don't use the rx) the cpu can handle the for loop things and function calls in the time the pio can write multiple bytes. The for loop has to wait for the pio to empty its FIFO every time instead of the sio waiting for the for loop to do their things. 
* The pio runs at cpu speed or cpu/2 speed(Not certain about that). so it's insanly fast. 
* If I figure out a way to use the DMA module it can write directly from memory to the PIO. So the core has to do nothing during the write.


