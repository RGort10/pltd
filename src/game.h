#ifndef GAME_H
#define GAME_H

/*
  The main program expects that there are two functions available for a game:
  uint16_t game_setup(frameBuffer_t *buf);
  void game_loop(frameBuffer_t *buf);

  The setup runs once and returns the number of milliseconds to wait after the setup (To show a start screen).
  The loop runs for each frame. You should only write to the framebuffer and NOT to the lcd directly. 
  The framebuffer get's written to the screen approximately every 11ms. Thats 90FPS. That should be enough
*/

// Debug is not placed in a subdirectory. That way it has access to all the hardware directly 
#ifdef GAME_IS_DEBUG
  #include "debug.h"
#endif

#ifdef GAME_IS_PONG
  #include "pong/pong.h"
#endif

#ifdef GAME_IS_TETRIS
  #include "tetris/tetris.h"
#endif


#endif