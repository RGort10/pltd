#ifndef PONG_BAT_H
#define PONG_BAT_H

#include <stdbool.h>
#include "../framebuffer.h"
#include "pongTypes.h"

#define PONG_BAT_SIZE_X PONG_SCREEN_MULTIPLIER * 2
#define PONG_BAT_SIZE_Y PONG_SCREEN_MULTIPLIER * 10
#define PONG_BAT_SPEED PONG_SCREEN_MULTIPLIER

void pong_moveBat(pong_bat_t *bat, bool up, bool down);
void pong_moveBatAnalog(pong_bat_t *bat, uint16_t analogValue);
void pong_renderBat(frameBuffer_t *buf, pong_bat_t *bat);

#endif