#ifndef LCD_H
#define LCD_H

#include <stdint.h>
#include <stdbool.h>

#include "framebuffer.h"

#define LCD_DELAY_COMMAND 0xFF

typedef struct {
  uint8_t cs;
  uint8_t dc;
  uint8_t rd;
  uint8_t wr;
  uint8_t reset;
  uint8_t dbStart;
} lcd_pins;

void lcd_setDataDirection(bool isOuput);
void lcd_setData(uint8_t value);
uint8_t lcd_getData(void);

void lcd_reset(void);
void lcd_init(uint8_t cs, uint8_t dc, uint8_t rd, uint8_t wr, uint8_t reset, uint8_t dbStart);

void lcd_write8(uint8_t value);
void lcd_writeRegister32(uint8_t r, uint32_t value);

void lcd_setAddrWindow(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void lcd_flood(uint16_t color, uint32_t len, bool usePIO);
void lcd_fillScreen(uint16_t color, bool usePIO);
void lcd_writeFrameBuffer(frameBuffer_t *buf);

#endif