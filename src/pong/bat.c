#include "bat.h"
#include "../util.h"

void pong_moveBat(pong_bat_t *bat, bool up, bool down) {
  if(up) bat->y += PONG_BAT_SPEED;
  if(down) bat->y -= PONG_BAT_SPEED;
  if(bat->y < PONG_BAT_SPEED) bat->y = PONG_BAT_SPEED;
  if(bat->y > PONG_SCREEN_HEIGHT - (PONG_BAT_SPEED + 1)) bat->y = PONG_SCREEN_HEIGHT - PONG_BAT_SPEED - 1;
}

void pong_moveBatAnalog(pong_bat_t *bat, uint16_t analogValue) {
  bat->y = map(analogValue, 0, 0xFFF, 0, PONG_SCREEN_HEIGHT);
}

void pong_renderBat(frameBuffer_t *buf, pong_bat_t *bat) {
  int32_t x1  = ((int32_t)bat->x - PONG_BAT_SIZE_X) / PONG_SCREEN_MULTIPLIER;
  int32_t y1  = ((int32_t)bat->y - PONG_BAT_SIZE_Y) / PONG_SCREEN_MULTIPLIER;
  uint32_t x2 = (bat->x + PONG_BAT_SIZE_X) / PONG_SCREEN_MULTIPLIER;
  uint32_t y2 = (bat->y + PONG_BAT_SIZE_Y) / PONG_SCREEN_MULTIPLIER;
  
  if(x1 < 0) x1 = 0;
  if(y1 < 0) y1 = 0;

  frameBuffer_fillRectangle(buf, x1, y1, x2, y2, FRAMEBUFFER_WHITE);
}