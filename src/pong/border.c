#include "border.h"

void pong_renderBorder(frameBuffer_t *buf) {
  frameBuffer_fillRectangle(buf, 0, 0, SCREEN_WIDTH - 1, 2, FRAMEBUFFER_GRAY);
  frameBuffer_fillRectangle(buf, 0, SCREEN_HEIGHT - 3, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1, FRAMEBUFFER_GRAY);
  frameBuffer_fillDashedRectangle(buf, SCREEN_WIDTH / 2 - 1, 0, SCREEN_WIDTH / 2 + 1, SCREEN_HEIGHT - 1, 2, FRAMEBUFFER_GRAY);
}