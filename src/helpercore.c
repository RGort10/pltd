#include "helpercore.h"
#include "lcd.h"
#include "pico/util/queue.h"
#include "pico/multicore.h"

#define NO_FRAMEBUFFER_USED 0xFF

frameBuffer_t frameBuffer0[SCREEN_WIDTH*SCREEN_HEIGHT];
frameBuffer_t frameBuffer1[SCREEN_WIDTH*SCREEN_HEIGHT];
uint8_t currentFrameBuffer = NO_FRAMEBUFFER_USED; 

queue_t helper_callQueue;
queue_t helper_resultsQueue;

void helper_coreEntry(void) {
  while(1) {
    helper_queueEntry_t entry;

    queue_remove_blocking(&helper_callQueue, &entry);

    switch(entry.command) {
      case GET_FRAMEBUFFER: 
        if(currentFrameBuffer = NO_FRAMEBUFFER_USED) {
          entry.frameBuffer = frameBuffer0;
          currentFrameBuffer = 0;
          queue_add_blocking(&helper_resultsQueue, &entry);
        } else {
          entry.frameBuffer = NULL;
          queue_add_blocking(&helper_resultsQueue, &entry);
        }
        break;

      case PAINT_FRAMEBUFFER: 
        if(currentFrameBuffer == NO_FRAMEBUFFER_USED) {
          entry.frameBuffer = NULL;
          queue_add_blocking(&helper_resultsQueue, &entry);
        } else {
          currentFrameBuffer = !currentFrameBuffer;
          entry.frameBuffer = currentFrameBuffer ? frameBuffer1 : frameBuffer0;
          queue_add_blocking(&helper_resultsQueue, &entry);
          lcd_writeFrameBuffer(currentFrameBuffer ? frameBuffer0 : frameBuffer1);
        }
        break;

      default: 
        entry.command = COMMAND_UNKNOWN;
        entry.frameBuffer = NULL;
        queue_add_blocking(&helper_resultsQueue, &entry);
        break;
    }
  }
} 