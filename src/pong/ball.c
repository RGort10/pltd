#include <stdlib.h>
#include "ball.h"
#include "bat.h"

bool pong_checkCollision(uint32_t x0_1, uint32_t y0_1, uint32_t x0_2, uint32_t y0_2, uint32_t x1_1, uint32_t y1_1, uint32_t x1_2, uint32_t y1_2) {
  if(x0_1 > x1_2) return false;
  if(x0_2 < x1_1) return false;
  if(y0_1 > y1_2) return false;
  if(y0_2 < y1_1) return false;

  return true;
}

void pong_moveBall(pong_ball_t *ball, pong_bat_t *bat0, pong_bat_t *bat1, pong_score_t *score) {
  // Upper half of bat
  if(pong_checkCollision( 
    ball->x - PONG_BALL_SIZE_X, ball->y - PONG_BALL_SIZE_Y, ball->x + PONG_BALL_SIZE_X, ball->y + PONG_BALL_SIZE_Y,
    bat0->x - PONG_BAT_SIZE_X,  bat0->y - PONG_BAT_SIZE_Y,  bat0->x,                    bat0->y)
  ) {
    if(ball->moveX < 0) ball->moveX *= -1;
    if(ball->moveY > 0) ball->moveY *= -1;
  }
  // Lower half of bat
  if(pong_checkCollision( 
    ball->x - PONG_BALL_SIZE_X, ball->y - PONG_BALL_SIZE_Y, ball->x + PONG_BALL_SIZE_X, ball->y + PONG_BALL_SIZE_Y,
    bat0->x,                    bat0->y,                    bat0->x + PONG_BAT_SIZE_X,  bat0->y + PONG_BAT_SIZE_Y)
  ) {
    if(ball->moveX < 0) ball->moveX *= -1;
    if(ball->moveY < 0) ball->moveY *= -1;
  }

  // Upper half of bat
  if(pong_checkCollision( 
    ball->x - PONG_BALL_SIZE_X, ball->y - PONG_BALL_SIZE_Y, ball->x + PONG_BALL_SIZE_X, ball->y + PONG_BALL_SIZE_Y,
    bat1->x - PONG_BAT_SIZE_X,  bat1->y - PONG_BAT_SIZE_Y,  bat1->x,                    bat1->y)
  ) {
    if(ball->moveX > 0) ball->moveX *= -1;
    if(ball->moveY > 0) ball->moveY *= -1;
  }
  // Lower half of bat
  if(pong_checkCollision( 
    ball->x - PONG_BALL_SIZE_X, ball->y - PONG_BALL_SIZE_Y, ball->x + PONG_BALL_SIZE_X, ball->y + PONG_BALL_SIZE_Y,
    bat1->x,                    bat1->y,                    bat1->x + PONG_BAT_SIZE_X,  bat1->y + PONG_BAT_SIZE_Y)
  ) {
    if(ball->moveX > 0) ball->moveX *= -1;
    if(ball->moveY < 0) ball->moveY *= -1;
  }

  ball->x += ball->moveX;
  ball->y += ball->moveY;
  
  if(ball->moveX < 0 && ball->x < -ball->moveX) {
    ball->moveX *= -1;
    ball->y = (rand() % SCREEN_HEIGHT) * PONG_SCREEN_MULTIPLIER;
    ball->moveY = (rand() & 1) ? 1 : -1;
    score->right++;
  }  
  if(ball->moveX > 0 && ball->x >= PONG_SCREEN_WIDTH - 1 - ball->moveX) {
    ball->moveX *= -1;
    ball->y = (rand() % SCREEN_HEIGHT) * PONG_SCREEN_MULTIPLIER;
    ball->moveY = (rand() & 1) ? 1 : -1;
    score->left++;
  }

  if(ball->y <= -ball->moveY + 1 || ball->y >= PONG_SCREEN_HEIGHT - 2 - ball->moveY) ball->moveY *= -1;
}

void pong_renderBall(frameBuffer_t *buf, pong_ball_t *ball) {
  int32_t x1  = (ball->x - PONG_BALL_SIZE_X) / PONG_SCREEN_MULTIPLIER;
  int32_t y1  = (ball->y - PONG_BALL_SIZE_Y) / PONG_SCREEN_MULTIPLIER;
  uint32_t x2 = (ball->x + PONG_BALL_SIZE_X) / PONG_SCREEN_MULTIPLIER;
  uint32_t y2 = (ball->y + PONG_BALL_SIZE_Y) / PONG_SCREEN_MULTIPLIER;
  
  if(x1 < 0) x1 = 0;
  if(y1 < 0) y1 = 0;

  frameBuffer_fillRectangle(buf, x1, y1, x2, y2, FRAMEBUFFER_WHITE);
}