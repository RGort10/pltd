#include "tetris.h"

uint16_t game_setup(frameBuffer_t *buf) {

  return 0;
}

void game_loop(frameBuffer_t *buf) {
  frameBuffer_fillBuffer(buf, FRAMEBUFFER_BLACK);

  frameBuffer_fillRectangle(buf, 20, 20, 60, 60, FRAMEBUFFER_RED);
  frameBuffer_fillRectangle(buf, 60, 20, 80, 60, FRAMEBUFFER_GREEN);
  frameBuffer_fillRectangle(buf, 80, 20, 100, 60, FRAMEBUFFER_BLUE);

}