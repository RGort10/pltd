#include "debug.h"
#include "input.h"

uint16_t game_setup(frameBuffer_t *buf) {

  return 0;
}

#define ADC_THRESHOLD 35
#define ADC_THRESHOLD1 40
#define ADC_THRESHOLD2 45
uint16_t adcPrevious = 0; 
uint16_t adcPrevious1 = 0; 
uint16_t adcPrevious2 = 0; 

void game_loop(frameBuffer_t *buf) {
  frameBuffer_fillBuffer(buf, FRAMEBUFFER_BLACK);

  input_inputs_t inputs = input_getInputs();
  

  frameBuffer_printChar(buf, 4, 4        , 'V', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8, 4    , 'A', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 2, 4, 'L', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 3, 4, '0', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 4, 4, ':', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 5, 4, ' ', FRAMEBUFFER_WHITE);
  frameBuffer_print_uint16(buf, 4 + 8 * 6, 4, inputs.analog.left, FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, 4, 14        , 'V', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8, 14    , 'A', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 2, 14, 'L', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 3, 14, '1', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 4, 14, ':', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 5, 14, ' ', FRAMEBUFFER_WHITE);
  frameBuffer_print_uint16(buf, 4 + 8 * 6, 14, 0, FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, 4, 24        , 'V', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8, 24    , 'A', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 2, 24, 'L', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 3, 24, '2', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 4, 24, ':', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 5, 24, ' ', FRAMEBUFFER_WHITE);
  frameBuffer_print_uint16(buf, 4 + 8 * 6, 24, 0, FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, 4, 34        , 'V', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8, 34    , 'A', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 2, 34, 'L', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 3, 34, '3', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 4, 34, ':', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 5, 34, ' ', FRAMEBUFFER_WHITE);
  frameBuffer_print_uint16(buf, 4 + 8 * 6, 34, 0, FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, 4, 44        , 'V', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8, 44    , 'A', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 2, 44, 'L', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 3, 44, '4', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 4, 44, ':', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 5, 44, ' ', FRAMEBUFFER_WHITE);
  frameBuffer_print_uint16(buf, 4 + 8 * 6, 44, 0, FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, 4, 54        , 'V', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8, 54    , 'A', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 2, 54, 'L', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 3, 54, '5', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 4, 54, ':', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 5, 54, ' ', FRAMEBUFFER_WHITE);
  frameBuffer_print_uint16(buf, 4 + 8 * 6, 54, 0, FRAMEBUFFER_WHITE);

  frameBuffer_printChar(buf, 4, 64        , 'V', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8, 64    , 'A', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 2, 64, 'L', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 3, 64, '6', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 4, 64, ':', FRAMEBUFFER_WHITE);
  frameBuffer_printChar(buf, 4 + 8 * 5, 64, ' ', FRAMEBUFFER_WHITE);
  frameBuffer_print_uint16(buf, 4 + 8 * 6, 64, 0, FRAMEBUFFER_WHITE);

}