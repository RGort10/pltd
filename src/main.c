/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/util/queue.h"
#include "pico/multicore.h"
#include "lcd.h"
#include "game.h"
#include "helpercore.h"
#include "input.h"

#define	RED     0xF800
#define	GREEN   0x07E0
#define	BLUE    0x001F
#define WHITE   0xFFFF

frameBuffer_t *frameBuffer;

int main() {
  helper_queueEntry_t entry;
  uint16_t sleepTime;

  gpio_init(25);
  gpio_set_dir(25, GPIO_OUT);

  input_setup();
  srand(0);

  lcd_init(4, 5, 7, 6, 3, 8);

  queue_init(&helper_callQueue, sizeof(helper_queueEntry_t), 2);
  queue_init(&helper_resultsQueue, sizeof(helper_queueEntry_t), 2);
  multicore_launch_core1(helper_coreEntry);

  entry = (helper_queueEntry_t){GET_FRAMEBUFFER, NULL};
  queue_add_blocking(&helper_callQueue, &entry);
  queue_remove_blocking(&helper_resultsQueue, &entry);
  frameBuffer = entry.frameBuffer;

  frameBuffer_fillBuffer(frameBuffer, FRAMEBUFFER_BLACK);

  sleepTime = game_setup(frameBuffer);

  entry = (helper_queueEntry_t){PAINT_FRAMEBUFFER, frameBuffer};
  queue_add_blocking(&helper_callQueue, &entry);
  queue_remove_blocking(&helper_resultsQueue, &entry);
  frameBuffer = entry.frameBuffer;

  sleep_ms(sleepTime);

  while(1) {
    game_loop(frameBuffer);
    
    entry = (helper_queueEntry_t){PAINT_FRAMEBUFFER, frameBuffer};
    queue_add_blocking(&helper_callQueue, &entry);
    queue_remove_blocking(&helper_resultsQueue, &entry);
    frameBuffer = entry.frameBuffer;
  }
}