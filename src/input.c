#include "input.h"
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/i2c.h"
#include "devices/mcp23017.h"
#include "hardware/adc.h"

input_analogInputs_t previousAnalogReadings;

void input_mcp23017_write_byte(uint8_t reg, uint8_t val) {
  uint8_t data[2] = {reg, val};
  i2c_write_blocking(i2c0, MCP23017_ADDRESS, data, 2, false);
}

uint16_t input_mcp23017_read_short(uint8_t reg) {
  uint8_t val[2];
  i2c_write_blocking(i2c0, MCP23017_ADDRESS, &reg, 1, true);
  i2c_read_blocking(i2c0, MCP23017_ADDRESS, val, 2, false);
  return (val[1] << 8) | val[0];
}

void input_setup(void) {
  gpio_init(20);
  gpio_init(21);
  gpio_pull_up(20);
  gpio_pull_up(21);
  gpio_set_function(20, GPIO_FUNC_I2C);
  gpio_set_function(21, GPIO_FUNC_I2C);
  
  i2c_init(i2c0, 100 * 1000);

  input_mcp23017_write_byte(MCP23017_IODIRA, 0xFF);
  input_mcp23017_write_byte(MCP23017_IODIRB, 0xFF);

  adc_init();
  adc_gpio_init(26);
  adc_gpio_init(27);
}

input_inputs_t input_getInputs(void) {
  input_inputs_t inputs;
  uint16_t analogInput;
  
  inputs.digital.raw = input_mcp23017_read_short(MCP23017_GPIOA);
  
  adc_select_input(0);
  analogInput = adc_read();
  if(analogInput < (INPUT_ANALOG_TRHESHOLD + 10)) previousAnalogReadings.left = 0;
  else if(
    analogInput < previousAnalogReadings.left - INPUT_ANALOG_TRHESHOLD 
    || analogInput > previousAnalogReadings.left + INPUT_ANALOG_TRHESHOLD
  ) previousAnalogReadings.left = analogInput;
  inputs.analog.left = previousAnalogReadings.left;

  adc_select_input(1);
  analogInput = adc_read();
  if(analogInput < (INPUT_ANALOG_TRHESHOLD + 10)) previousAnalogReadings.right = 0;
  else if(
    analogInput < previousAnalogReadings.right - INPUT_ANALOG_TRHESHOLD 
    || analogInput > previousAnalogReadings.right + INPUT_ANALOG_TRHESHOLD
  ) previousAnalogReadings.right = analogInput;
  inputs.analog.right = previousAnalogReadings.right;

  return inputs;
}