#ifndef FONT_H
#define FONT_H

#include <stdint.h>

#define FONT_HEIGHT 8
#define FONT_WIDTH 8

const uint8_t* font_getChar(char c);

#endif