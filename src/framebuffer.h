#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <stdint.h>

#define SCREEN_WIDTH 120
#define SCREEN_HEIGHT 80

typedef uint16_t frameBuffer_t;

// Color is defined as R4..0 G5..0 B4..0
#define FRAMEBUFFER_BLACK (uint16_t)0x0000 
#define FRAMEBUFFER_WHITE (uint16_t)0xFFFF 
#define	FRAMEBUFFER_RED   (uint16_t)0xF800
#define	FRAMEBUFFER_GREEN (uint16_t)0x07E0
#define	FRAMEBUFFER_BLUE  (uint16_t)0x001F
#define FRAMEBUFFER_GRAY  (uint16_t)0x94B2

void frameBuffer_fillBuffer(frameBuffer_t *buf, uint16_t color);
void frameBuffer_fillRectangle(frameBuffer_t *buf, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
void frameBuffer_fillDashedRectangle(frameBuffer_t *buf, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t dashLength, uint16_t color);
void frameBuffer_printChar(frameBuffer_t *buf, uint16_t x1, uint16_t y1, char c, uint16_t color);
void frameBuffer_print_uint16(frameBuffer_t *buf, uint16_t x1, uint16_t y1, uint16_t val, uint16_t color);
void frameBuffer_printCharXL(frameBuffer_t *buf, uint16_t x1, uint16_t y1, char c, uint16_t color);

#endif