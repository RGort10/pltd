#include "lcd.h"
#include "devices/hx8357d.h"
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "send_2_4x_byte.pio.h"

lcd_pins pins;

void gpio_put_delay(uint32_t gpio, bool value) {
  if(value) {
    sio_hw->gpio_set = 1 << gpio;
  } else {
    sio_hw->gpio_clr = 1 << gpio;
  }
}

void lcd_setDataDirection(bool isOuput) {
  gpio_set_dir_masked(0xFF << pins.dbStart, isOuput ? 0xFF << pins.dbStart : 0);
}

void lcd_setData(uint8_t value) {
  sio_hw->gpio_set = value << pins.dbStart;
  sio_hw->gpio_clr = (~value & 0xFF) << pins.dbStart;
  // gpio_put_masked(0xFF << pins.dbStart, value << pins.dbStart);
}

uint8_t lcd_getData(void) {
  return (gpio_get_all() >> pins.dbStart) & 0xFF;
}

void lcd_write8(uint8_t value) {
  sio_hw->gpio_set = value << pins.dbStart;
  sio_hw->gpio_clr = ((~value & 0xFF) << pins.dbStart) | (1 << pins.wr);
  asm("nop");
  sio_hw->gpio_set = 1 << pins.wr;
}

void lcd_write32_2(uint32_t value, uint32_t value2) {
  uint32_t wr = 1 << pins.wr;
  uint32_t invValue = ~value;
  sio_hw->gpio_set = ((value >> 24) & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = (((invValue >> 24) & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;
  
  sio_hw->gpio_set = ((value >> 16) & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = (((invValue >> 16) & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;
  
  sio_hw->gpio_set = ((value >> 8) & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = (((invValue >> 8) & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;
  
  sio_hw->gpio_set = (value & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = ((invValue & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;

  uint32_t invValue2 = ~value2;
  sio_hw->gpio_set = ((value2 >> 24) & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = (((invValue2 >> 24) & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;
  
  sio_hw->gpio_set = ((value2 >> 16) & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = (((invValue2 >> 16) & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;
  
  sio_hw->gpio_set = ((value2 >> 8) & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = (((invValue2 >> 8) & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;
  
  sio_hw->gpio_set = (value2 & 0xFF) << pins.dbStart;
  sio_hw->gpio_clr = ((invValue2 & 0xFF) << pins.dbStart) | wr;
  asm("nop");
  sio_hw->gpio_set = wr;
}

void lcd_writeRegister32(uint8_t r, uint32_t value) {
  gpio_put_delay(pins.cs, 0);
  gpio_put_delay(pins.dc, 0);
  lcd_write8(r);

  sleep_us(10);

  gpio_put_delay(pins.dc, 1);
  lcd_write8(value >> 24);
  sleep_us(10);
  lcd_write8(value >> 16);
  sleep_us(10);
  lcd_write8(value >> 8);
  sleep_us(10);
  lcd_write8(value);
  sleep_us(10);

  gpio_put_delay(pins.cs, 1);
}

static const uint8_t HX8357D_INIT_PROGRAM[] = {
    HX8357_SWRESET,
    0,
    HX8357D_SETC,
    3,
    0xFF,
    0x83,
    0x57,
    LCD_DELAY_COMMAND,
    250,
    HX8357_SETRGB,
    4,
    0x00,
    0x00,
    0x06,
    0x06,
    HX8357D_SETCOM,
    1,
    0x25, // -1.52V
    HX8357_SETOSC,
    1,
    0x68, // Normal mode 70Hz, Idle mode 55 Hz
    HX8357_SETPANEL,
    1,
    0x05, // BGR, Gate direction swapped
    HX8357_SETPWR1,
    6,
    0x00,
    0x15,
    0x1C,
    0x1C,
    0x83,
    0xAA,
    HX8357D_SETSTBA,
    6,
    0x50,
    0x50,
    0x01,
    0x3C,
    0x1E,
    0x08,
    // MEME GAMMA HERE
    HX8357D_SETCYC,
    7,
    0x02,
    0x40,
    0x00,
    0x2A,
    0x2A,
    0x0D,
    0x78,
    HX8357_COLMOD,
    1,
    0x55,
    HX8357_MADCTL,
    1,
    0xC0,
    HX8357_TEON,
    1,
    0x00,
    HX8357_TEARLINE,
    2,
    0x00,
    0x02,
    HX8357_SLPOUT,
    0,
    LCD_DELAY_COMMAND,
    150,
    HX8357_DISPON,
    0,
    LCD_DELAY_COMMAND,
    50
};

void lcd_reset(void) {
  gpio_put_delay(pins.cs, 1);
  gpio_put_delay(pins.rd, 1);
  gpio_put_delay(pins.wr, 1);

  gpio_put_delay(pins.reset, 0);
  sleep_ms(50);
  gpio_put_delay(pins.reset, 1);
  sleep_ms(50);

  gpio_put_delay(pins.cs, 0);
  gpio_put_delay(pins.dc, 0);
  lcd_write8(0);
  lcd_write8(0);
  lcd_write8(0);
  lcd_write8(0);
  gpio_put_delay(pins.dc, 1);
  gpio_put_delay(pins.cs, 1);
}

void lcd_init(uint8_t cs, uint8_t dc, uint8_t rd, uint8_t wr, uint8_t reset, uint8_t dbStart) {
  uint32_t i = 0;

  pins = (lcd_pins){cs, dc, rd, wr, reset, dbStart};

  uint32_t offset = pio_add_program(pio0, &send_2_4x_byte_program);
  send_2_4x_byte_program_init(pio0, 0, offset, pins.dbStart, pins.wr);
  pio_sm_set_clkdiv(pio0, 0, 1.0);
  pio_sm_clear_fifos(pio0, 0);
  pio_sm_set_enabled(pio0, 0, false);

  gpio_init_mask(0xFF << pins.dbStart);
  gpio_init(pins.cs);
  gpio_init(pins.dc);
  gpio_init(pins.rd);
  gpio_init(pins.wr);
  gpio_init(pins.reset);
  for(int j = 0; j < 8; j++) gpio_set_slew_rate(pins.dbStart + j, GPIO_SLEW_RATE_FAST);
  gpio_set_slew_rate(pins.cs, GPIO_SLEW_RATE_FAST);
  gpio_set_slew_rate(pins.dc, GPIO_SLEW_RATE_FAST);
  gpio_set_slew_rate(pins.rd, GPIO_SLEW_RATE_FAST);
  gpio_set_slew_rate(pins.wr, GPIO_SLEW_RATE_FAST);
  gpio_put_delay(pins.cs, 1);
  gpio_put_delay(pins.dc, 1);
  gpio_put_delay(pins.rd, 1);
  gpio_put_delay(pins.wr, 1);
  gpio_put_delay(pins.reset, 1);
  lcd_setDataDirection(GPIO_OUT);
  gpio_set_dir(pins.cs, GPIO_OUT);
  gpio_set_dir(pins.dc, GPIO_OUT);
  gpio_set_dir(pins.rd, GPIO_OUT);
  gpio_set_dir(pins.wr, GPIO_OUT);
  gpio_set_dir(pins.reset, GPIO_OUT);

  lcd_reset();

  while(i < sizeof(HX8357D_INIT_PROGRAM)) {
    uint8_t reg = HX8357D_INIT_PROGRAM[i++];
    uint8_t len = HX8357D_INIT_PROGRAM[i++];

    if(reg == LCD_DELAY_COMMAND) {
      sleep_ms(len);
    } else {
      gpio_put_delay(pins.cs, 0);
      gpio_put_delay(pins.dc, 0);
      lcd_write8(reg);
      gpio_put_delay(pins.dc, 1);

      for(uint8_t d = 0; d < len; d++) {
        lcd_write8(HX8357D_INIT_PROGRAM[i++]);
      }

      gpio_put_delay(pins.cs, 1);
    }
  }
}

void lcd_setAddrWindow(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2) {
  gpio_put_delay(pins.cs, 0);

  lcd_writeRegister32(HX8357_COLADDRSET, (x1 << 16) | x2);
  lcd_writeRegister32(HX8357_PAGEADDRSET, (y1 << 16) | y2);

  gpio_put_delay(pins.cs, 1);
}

void lcd_flood(uint16_t color, uint32_t len, bool usePIO) {
  uint16_t blocks;
  uint8_t hi = color >> 8, lo = color & 0xFF;

  gpio_put_delay(pins.cs, 0);
  gpio_put_delay(pins.dc, 0);
  lcd_write8(HX8357_RAMWR);

  gpio_put_delay(pins.dc, 1);
  uint32_t col = (color << 16) | color;
  if(usePIO) {
    gpio_set_function(pins.wr, GPIO_FUNC_PIO0);
    for(int p = 0; p < 8; p ++) {
      gpio_set_function(pins.dbStart + p, GPIO_FUNC_PIO0);
    }
    pio_sm_set_enabled(pio0, 0, true);
    for(uint32_t i = 0; i < len; i+=4) {
      pio_sm_put_blocking(pio0, 0, (color >> 8) | ((color & 0xFF) << 8));
      pio_sm_put_blocking(pio0, 0, (color >> 8) | ((color & 0xFF) << 8));
      pio_sm_put_blocking(pio0, 0, (color >> 8) | ((color & 0xFF) << 8));
      pio_sm_put_blocking(pio0, 0, (color >> 8) | ((color & 0xFF) << 8));
    }
    pio_sm_set_enabled(pio0, 0, false);
    gpio_set_function(pins.wr, GPIO_FUNC_SIO);
    for(int p = 0; p < 8; p ++) {
      gpio_set_function(pins.dbStart + p, GPIO_FUNC_SIO);
    }
  } else {
    for(uint32_t i = 0; i < len; i+=4) {
      lcd_write32_2(col, col);
      // lcd_write8(hi);
      // lcd_write8(lo);
      // lcd_write8(hi);
      // lcd_write8(lo);
      // lcd_write8(hi);
      // lcd_write8(lo);
      // lcd_write8(hi);
      // lcd_write8(lo);
    }
  }
  gpio_put_delay(pins.cs, 1);
}

void lcd_fillScreen(uint16_t color, bool usePIO) {
  lcd_setAddrWindow(0, 0, 319, 479);
  lcd_flood(color, 320 * 480 / 4, usePIO);
}

void lcd_writeFrameBuffer(frameBuffer_t *buf) {
  lcd_setAddrWindow(0, 0, 319, 479);
  gpio_put_delay(pins.cs, 0);
  gpio_put_delay(pins.dc, 0);
  lcd_write8(HX8357_RAMWR);

  gpio_put_delay(pins.dc, 1);

  gpio_set_function(pins.wr, GPIO_FUNC_PIO0);
  lcd_setData(0);
  for(uint32_t i = 0; i < 8; i++) {
    gpio_set_function(pins.dbStart + i, GPIO_FUNC_PIO0);
  }
  pio_sm_set_enabled(pio0, 0, true);

  for(int32_t x = SCREEN_WIDTH - 1; x >= 0; x--) {
    for(uint32_t i = 0; i < 4; i++) {
      for(uint32_t y = 0; y < SCREEN_HEIGHT; y++) {
        pio_sm_put_blocking(pio0, 0, buf[y*SCREEN_WIDTH + x]);
      }
    }
  }

  while(!pio_sm_is_tx_fifo_empty(pio0, 0)) continue; // Wait till fifo empty
  sleep_us(2);

  pio_sm_set_enabled(pio0, 0, false);
  gpio_set_function(pins.wr, GPIO_FUNC_SIO);
  for(uint32_t i = 0; i < 8; i++) {
    gpio_set_function(pins.dbStart + i, GPIO_FUNC_SIO);
  }

  gpio_put_delay(pins.cs, 1);
}